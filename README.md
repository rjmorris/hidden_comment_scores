This was completed as an entry in the [September 2019 Reddit DataIsBeautiful visualization contest]( https://www.reddit.com/r/dataisbeautiful/comments/d0oh2m/battle_dataviz_battle_for_the_month_of_september/). The challenge was to visualize a dataset of comment scores from the `r/Formula1` subreddit, where scores were hidden from readers for varying lengths of time (60 minutes, 30 minutes, or not hidden at all).

I'll get right to the visualization I submitted:

![outliers by hidden time](outliers.png)

The main point I tried to convey is that outlier scores (scores that are extremely high or low) happened less frequently when the score was hidden for longer. A secondary point is that the number of outliers decreased over time for the downvoted posts but not for the upvoted posts.

A few things I could have improved:

- The y-axis doesn't start at 0! I didn't notice that until after I'd submitted the visualization to the contest. That's bad practice in my book, and I really wish I hadn't overlooked it.
- I could have added some notes to provide context. Candidates would be:

    - Background info about the data.
    - My definition of upvoted and downvoted posts. I used a post's score at 120 minutes to classify the post as upvoted (score at 120m > 1) or downvoted (score at 120m < 1).
    - My definition of an outlier. If a score was more than 3 standard deviations from the mean of all scores at that time point, I considered it an outlier.

- I could have added confidence intervals. I thought about this but decided against it, because I didn't have any information on the sampling procedure.

The [exploratory.ipynb](exploratory.ipynb) notebook contains my exploratory journey through the data leading up to the plot I submitted.

One of my earlier plots was the average score over time, separating out the upvoted and downvoted posts:

![average score over time](avg_score.png)

I nearly stopped here, but one thing bothered me about this plot: For the first 30 minutes, the average score for posts that were hidden for 60 minutes were different from the average score for posts that were hidden for 30 minutes. Theoretically, they should have been the same, since the scores for both sets of posts were hidden during that timespan. As part of my investigation into that, I looked into outliers. I believe the proportion of outliers drives much of the differences between the hidden-time groups, so that's why I chose to submit the outlier plot. The outlier plot still shows those same differences in the first 30 minutes, though, and I stopped before getting to the bottom of that discrepancy.
